package co.com.ingenieria_soft.controlador;

import co.com.ingenieria_soft.entidades.Factura;
import co.com.ingenieria_soft.controlador.util.JsfUtil;
import co.com.ingenieria_soft.controlador.util.JsfUtil.PersistAction;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("facturaController")
@SessionScoped
public class FacturaController implements Serializable {

    @EJB
    private co.com.ingenieria_soft.controlador.FacturaFacade ejbFacade;
    private List<Factura> items = null;
    private Factura selected = new Factura();
    private Factura nuevo = new Factura();

    public FacturaController() {
    }

    public FacturaController(FacturaFacade ejbFacade) {
        this.ejbFacade = ejbFacade;
    }
    
    public Factura getNuevo() {
        return nuevo;
    }

    public void setNuevo(Factura nuevo) {
        this.nuevo = nuevo;
    }

    public Factura getSelected() {
        return selected;
    }

    public void setSelected(Factura selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private FacturaFacade getFacade() {
        return ejbFacade;
    }

    public Factura prepareCreate() {
        selected = new Factura();
        initializeEmbeddableKey();
        return selected;
    }

    public void create(Factura aux) {

        if (!JsfUtil.isValidationFailed()) {
            //Poner valor por defecto al ID de cada registros que se crea
            //(Tamaño de la lista + 1), se debe convertir a short
            //selected.setId((short) (items.size() + 1));
//            selected = aux;

//            //Establecer valor del IVA siempre en 19%
//            Float iva = new Float(0.19);
            selected.setIva(aux.getIva());
//
//            //Calcular valor total
            Double valorTotal = aux.getServicioId().getHabitacion().getTipoHabitacion().getPrecio()
                    + (aux.getServicioId().getProductoAdicional().getPrecio()
                    * aux.getServicioId().getProductoAdicional().getCantidad());
//
//            //Limitar decimales a 2
            BigDecimal bd = new BigDecimal(valorTotal);
            bd = bd.setScale(2, RoundingMode.HALF_UP);
//
            selected.setValorTotal(bd.doubleValue());
//
//            //Calcular valor neto
            Double valorNeto = (aux.getIva() * valorTotal) + valorTotal;
//
//            //Limitar decimales a 2
            BigDecimal bd1 = new BigDecimal(valorNeto);
            bd1 = bd1.setScale(2, RoundingMode.HALF_UP);
//
            selected.setValorNeto(bd1.doubleValue());
//
//            //Establecer fecha de generación de la factura como fecha actual
            Date fecha = new Date();
            selected.setFecha(fecha);
//
//            //Establecer estado de la factura en "SI" por defecto
            selected.setEstado("Si");

            selected.setCliente(aux.getCliente());
            selected.setUsuarioId(aux.getUsuarioId());
            selected.setServicioId(aux.getServicioId());

            items = null;    // Invalidate list of items to trigger re-query.
        }
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("FacturaCreated"));
    }

    public void inactivarFactura() {
        if (selected.getEstado().equals("Si")) {
            selected.setEstado("No");
        } else {
            selected.setEstado("Si");
        }

        update();
    }

    public void inactivarFacturaSelected() {
        ServicioController servicio = new ServicioController();
        
        Factura temp = servicio.getFacturaSelected();
        
        if (temp.getEstado().equals("Si")) {
            temp.setEstado("No");
            ejbFacade.find((short) temp.getId()).setEstado("No");
        } else {
            temp.setEstado("Si");
            ejbFacade.find((short) temp.getId()).setEstado("Si");
        }

        update();
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("FacturaUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("FacturaDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Factura> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Factura getFactura(java.lang.Short id) {
        return getFacade().find(id);
    }

    public List<Factura> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Factura> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Factura.class)
    public static class FacturaControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            FacturaController controller = (FacturaController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "facturaController");
            return controller.getFactura(getKey(value));
        }

        java.lang.Short getKey(String value) {
            java.lang.Short key;
            key = Short.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Short value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Factura) {
                Factura o = (Factura) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Factura.class.getName()});
                return null;
            }
        }

    }

}
