package co.com.ingenieria_soft.controlador;

import co.com.ingenieria_soft.entidades.Habitacion;
import co.com.ingenieria_soft.controlador.util.JsfUtil;
import co.com.ingenieria_soft.controlador.util.JsfUtil.PersistAction;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("habitacionController")
@SessionScoped
public class HabitacionController implements Serializable {

    @EJB
    private co.com.ingenieria_soft.controlador.HabitacionFacade ejbFacade;
    private List<Habitacion> items = null;
    private Habitacion selected = new Habitacion();
    private Habitacion nuevo = new Habitacion();

    public HabitacionController() {
    }

    public Habitacion getNuevo() {
        return nuevo;
    }

    public void setNuevo(Habitacion nuevo) {
        this.nuevo = nuevo;
    }
    
    

    public Habitacion getSelected() {
        return selected;
    }

    public void setSelected(Habitacion selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private HabitacionFacade getFacade() {
        return ejbFacade;
    }

    public Habitacion prepareCreate() {
        selected = new Habitacion();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {

        if (!JsfUtil.isValidationFailed()) {
            //Poner valor por defecto al ID de cada registros que se crea
            //(Tamaño de la lista + 1), se debe convertir a short
            //selected.setId((short) (items.size() + 1));
            
            selected = nuevo;
            
            items = null;    // Invalidate list of items to trigger re-query.
        }
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("HabitacionCreated"));
    }

        public void inactivarHabitacion() {
        if (selected.getEstado().equals("Si")) {
            selected.setEstado("No");
        } else {
            selected.setEstado("Si");
        }

        update();
    }
    
    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("HabitacionUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("HabitacionDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Habitacion> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Habitacion getHabitacion(java.lang.Short id) {
        return getFacade().find(id);
    }

    public List<Habitacion> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Habitacion> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Habitacion.class)
    public static class HabitacionControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            HabitacionController controller = (HabitacionController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "habitacionController");
            return controller.getHabitacion(getKey(value));
        }

        java.lang.Short getKey(String value) {
            java.lang.Short key;
            key = Short.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Short value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Habitacion) {
                Habitacion o = (Habitacion) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Habitacion.class.getName()});
                return null;
            }
        }

    }

}
