/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.ingenieria_soft.controlador;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import co.com.ingenieria_soft.entidades.Usuario;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author Jhonatan
 */
@Named(value = "loginController")
@SessionScoped
public class LoginController implements Serializable {

    private String nombreUsuario;
    private String password;
    private Usuario usuarioLogueado;
    @EJB
    private UsuarioFacade conUsuario;

    /**
     * Creates a new instance of LoginController
     */
    public LoginController() {
    }

    public UsuarioFacade getConUsuario() {
        return conUsuario;
    }

    public void setConUsuario(UsuarioFacade conUsuario) {
        this.conUsuario = conUsuario;
    }
    

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Usuario getUsuarioLogueado() {
        return usuarioLogueado;
    }

    public void setUsuarioLogueado(Usuario usuarioLogueado) {
        this.usuarioLogueado = usuarioLogueado;
    }

    public String autenticarUsuario() {

        usuarioLogueado = conUsuario.encontrarUsuarioPorCorreo(nombreUsuario);

        if (usuarioLogueado != null) {

            if (usuarioLogueado.getPassword().equals(password)) {
                
                if (usuarioLogueado.getEstado().equalsIgnoreCase("Si")){
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Bienvenid@ " + usuarioLogueado.getNombre(), "Bienvenid@ " + usuarioLogueado.getNombre()));
                    return "ingresar";
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usuario inactivo", "Usuario inactivo"));
                }
                        
                
                
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Contraseña incorrecta", "Contraseña incorrecta"));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "El usuario no existe", "El usuario no existe"));
        }
        return null;

    }
    
    public String cerrarSesion(){
        usuarioLogueado =  null;
        nombreUsuario = "";
        password = "";
        return "cerrar";
    }

}
