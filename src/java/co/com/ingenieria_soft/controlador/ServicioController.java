package co.com.ingenieria_soft.controlador;

import co.com.ingenieria_soft.entidades.Servicio;
import co.com.ingenieria_soft.controlador.util.JsfUtil;
import co.com.ingenieria_soft.controlador.util.JsfUtil.PersistAction;
import co.com.ingenieria_soft.entidades.Factura;
import co.com.ingenieria_soft.entidades.Habitacion;
import co.com.ingenieria_soft.entidades.Usuario;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.swing.JOptionPane;
import org.primefaces.event.CloseEvent;

@Named("servicioController")
@SessionScoped
public class ServicioController implements Serializable {

    @EJB
    private co.com.ingenieria_soft.controlador.ServicioFacade ejbFacade;
    @EJB
    private co.com.ingenieria_soft.controlador.HabitacionFacade ejbHabitacion;
    @EJB
    private co.com.ingenieria_soft.controlador.FacturaFacade ejbFactura;
    private List<Servicio> items = null;
    Factura facturaSelected = new Factura();
    private Servicio selected = new Servicio();
    private Servicio nuevo = new Servicio();
    private String nombreCliente = "";
    private int numHabitacion = 0;
    @EJB
    private UsuarioFacade conUsuario;

    public ServicioController() {
    }

    public int getNumHabitacion() {
        return numHabitacion;
    }

    public void setNumHabitacion(int numHabitacion) {
        this.numHabitacion = numHabitacion;
    }

    public ServicioFacade getEjbFacade() {
        return ejbFacade;
    }

    public void setEjbFacade(ServicioFacade ejbFacade) {
        this.ejbFacade = ejbFacade;
    }

    public FacturaFacade getEjbFactura() {
        return ejbFactura;
    }

    public void setEjbFactura(FacturaFacade ejbFactura) {
        this.ejbFactura = ejbFactura;
    }

    public Factura getFacturaSelected() {
        return facturaSelected;
    }

    public void setFacturaSelected(Factura facturaSelected) {
        this.facturaSelected = facturaSelected;
    }

    public UsuarioFacade getConUsuario() {
        return conUsuario;
    }

    public void setConUsuario(UsuarioFacade conUsuario) {
        this.conUsuario = conUsuario;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public Servicio getNuevo() {
        return nuevo;
    }

    public void setNuevo(Servicio nuevo) {
        this.nuevo = nuevo;
    }

    public Servicio getSelected() {
        return selected;
    }

    public void setSelected(Servicio selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private ServicioFacade getFacade() {
        return ejbFacade;
    }

    public Servicio prepareCreate() {
        selected = new Servicio();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {

        if (!JsfUtil.isValidationFailed()) {
            //Poner valor por defecto al ID de cada registros que se crea
            //(Tamaño de la lista + 1), se debe convertir a short
            //selected.setId((short) (items.size() + 1));
            selected = nuevo;

            //Establecer checkin a la hora actual
            Date fechaActual = new Date();
            selected.setCheckIn(fechaActual);

            //Establecer estado de la habitacion en NO DISPONIBLE
            HabitacionController temporal = new HabitacionController();
            temporal.setEmbeddableKeys();
            temporal.initializeEmbeddableKey();
            temporal.setSelected(ejbHabitacion.find(selected.getHabitacion().getId()));
            temporal.getSelected().setEstado("No");
            temporal.update();
//        ejbHabitacion.find(selected.getHabitacion().getId()).setEstado("No");

            items = null;    // Invalidate list of items to trigger re-query.
        }

        if (selected.getHabitacion().getEstado().equalsIgnoreCase("No")) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Habitación NO disponible", "Habitación NO disponible"));
        } else {
            persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("ServicioCreated"));
        }
    }

    public void update() {

        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("ServicioUpdated"));

    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("ServicioDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void detalleFactura() {
        List<Factura> listaFacturas = new ArrayList<>();

        listaFacturas = ejbFactura.findAll();

        for (Factura aux : listaFacturas) {
            if (aux.getServicioId().getId() == selected.getId()) {
                facturaSelected = aux;
            }
        }

    }

    public void addMessage(String summary, String detail) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void finalizarFactura() {

        //Objetos para crear factura
        FacturaController nuevaFactura = new FacturaController();
        Factura temp = new Factura();

        //establecer codigo del servicio
        temp.setServicioId(selected);

        //Establecer valor del IVA siempre en 19%
        Float iva = new Float(0.19);
        temp.setIva(iva);

        //Calcular valor total
        Double valorTotal = temp.getServicioId().getHabitacion().getTipoHabitacion().getPrecio()
                + (temp.getServicioId().getProductoAdicional().getPrecio()
                * temp.getServicioId().getProductoAdicional().getCantidad());

        //Limitar decimales a 2
        BigDecimal bd = new BigDecimal(valorTotal);
        bd = bd.setScale(2, RoundingMode.HALF_UP);

        temp.setValorTotal(bd.doubleValue());

        //Calcular valor neto
        Double valorNeto = (temp.getIva() * valorTotal) + valorTotal;

        //Limitar decimales a 2
        BigDecimal bd1 = new BigDecimal(valorNeto);
        bd1 = bd1.setScale(2, RoundingMode.HALF_UP);

        temp.setValorNeto(bd1.doubleValue());

        //Establecer fecha de generación de la factura como fecha actual
        Date fecha = new Date();
        temp.setFecha(fecha);

        //Establecer estado de la factura en "SI" por defecto
        temp.setEstado("Si");

        //Establecer el nombre del cliente
        temp.setCliente(nombreCliente);

        //Establecer generador de la factura
        Usuario usuarioLogueado = conUsuario.find((short) 1);
        temp.setUsuarioId(usuarioLogueado);

        //nuevaFactura.setSelected(temp);
        //Establecer fecha de finalizacion del servicio
        temp.getServicioId().setCheckOut(fecha);

        //Crear factura
        ejbFactura.create(temp);
        //nuevaFactura.create(temp);

        //Establecer estado de la habitacion en DISPONIBLE
//        selected.getHabitacion().setEstado("Si");
        update();

    }

    public void cambiarEstadoFactura() {
        FacturaController facturaTemp = new FacturaController(ejbFactura);

        if (facturaSelected.getEstado().equalsIgnoreCase("Si")) {
            ejbFactura.find(facturaSelected.getId()).setEstado("No");
        } else {
            ejbFactura.find((short) facturaSelected.getId()).setEstado("Si");

        }

        facturaTemp.update();

    }

    public List<Servicio> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Servicio getServicio(java.lang.Short id) {
        return getFacade().find(id);
    }

    public List<Servicio> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Servicio> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Servicio.class)
    public static class ServicioControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ServicioController controller = (ServicioController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "servicioController");
            return controller.getServicio(getKey(value));
        }

        java.lang.Short getKey(String value) {
            java.lang.Short key;
            key = Short.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Short value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Servicio) {
                Servicio o = (Servicio) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Servicio.class.getName()});
                return null;
            }
        }

    }

}
