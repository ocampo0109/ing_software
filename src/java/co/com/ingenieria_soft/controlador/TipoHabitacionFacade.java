/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.ingenieria_soft.controlador;

import co.com.ingenieria_soft.entidades.TipoHabitacion;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Jhonatan
 */
@Stateless
public class TipoHabitacionFacade extends AbstractFacade<TipoHabitacion> {

    @PersistenceContext(unitName = "ingenieria_softPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TipoHabitacionFacade() {
        super(TipoHabitacion.class);
    }
    
}
