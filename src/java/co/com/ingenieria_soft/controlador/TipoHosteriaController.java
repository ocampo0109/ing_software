package co.com.ingenieria_soft.controlador;

import co.com.ingenieria_soft.entidades.TipoHosteria;
import co.com.ingenieria_soft.controlador.util.JsfUtil;
import co.com.ingenieria_soft.controlador.util.JsfUtil.PersistAction;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("tipoHosteriaController")
@SessionScoped
public class TipoHosteriaController implements Serializable {

    @EJB
    private co.com.ingenieria_soft.controlador.TipoHosteriaFacade ejbFacade;
    private List<TipoHosteria> items = null;
    private TipoHosteria selected = new TipoHosteria();
    private TipoHosteria nuevo = new TipoHosteria();

    public TipoHosteriaController() {
    }

    public TipoHosteria getNuevo() {
        return nuevo;
    }

    public void setNuevo(TipoHosteria nuevo) {
        this.nuevo = nuevo;
    }
    
    

    public TipoHosteria getSelected() {
        return selected;
    }

    public void setSelected(TipoHosteria selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private TipoHosteriaFacade getFacade() {
        return ejbFacade;
    }

    public TipoHosteria prepareCreate() {
        selected = new TipoHosteria();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {

        if (!JsfUtil.isValidationFailed()) {
            //Poner valor por defecto al ID de cada registros que se crea
            //(Tamaño de la lista + 1), se debe convertir a short
            //selected.setId((short) (items.size() + 1));
            selected = nuevo;

            items = null;    // Invalidate list of items to trigger re-query.
        }
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("TipoHosteriaCreated"));
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("TipoHosteriaUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("TipoHosteriaDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<TipoHosteria> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public TipoHosteria getTipoHosteria(java.lang.Short id) {
        return getFacade().find(id);
    }

    public List<TipoHosteria> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<TipoHosteria> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = TipoHosteria.class)
    public static class TipoHosteriaControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            TipoHosteriaController controller = (TipoHosteriaController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "tipoHosteriaController");
            return controller.getTipoHosteria(getKey(value));
        }

        java.lang.Short getKey(String value) {
            java.lang.Short key;
            key = Short.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Short value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof TipoHosteria) {
                TipoHosteria o = (TipoHosteria) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), TipoHosteria.class.getName()});
                return null;
            }
        }

    }

}
