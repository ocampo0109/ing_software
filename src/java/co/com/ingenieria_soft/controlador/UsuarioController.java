package co.com.ingenieria_soft.controlador;

import co.com.ingenieria_soft.entidades.Usuario;
import co.com.ingenieria_soft.controlador.util.JsfUtil;
import co.com.ingenieria_soft.controlador.util.JsfUtil.PersistAction;
import co.com.ingenieria_soft.entidades.Suscripcion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("usuarioController")
@SessionScoped
public class UsuarioController implements Serializable {

    @EJB
    private co.com.ingenieria_soft.controlador.UsuarioFacade ejbFacade;
    @EJB
    private co.com.ingenieria_soft.controlador.TipoUsuarioFacade ejbTipoUsuario;
    @EJB
    private co.com.ingenieria_soft.controlador.SuscripcionFacade ejbSuscripcion;
    private List<Usuario> items = null;
    private Usuario selected = new Usuario();
    private Usuario nuevo = new Usuario();
    private String nombre;
    private String nombreHosteria;
    private String nombreTipoUsuario;

    public UsuarioController() {
    }

    public String getNombreTipoUsuario() {
        return nombreTipoUsuario;
    }

    public void setNombreTipoUsuario(String nombreTipoUsuario) {
        this.nombreTipoUsuario = nombreTipoUsuario;
    }

    public Usuario getNuevo() {
        return nuevo;
    }

    public void setNuevo(Usuario nuevo) {
        this.nuevo = nuevo;
    }

    public String getNombreHosteria() {
        return nombreHosteria;
    }

    public void setNombreHosteria(String nombreHosteria) {
        this.nombreHosteria = nombreHosteria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Usuario getSelected() {
        return selected;
    }

    public void setSelected(Usuario selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private UsuarioFacade getFacade() {
        return ejbFacade;
    }

    private SuscripcionFacade getSuscripcionFacade() {
        return ejbSuscripcion;
    }

    public Usuario prepareCreate() {
        selected = new Usuario();
        //nuevo = new Usuario();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        if (!JsfUtil.isValidationFailed()) {
            //Llamar valores ingresados por el usuario a la variable interna que maneja JAVA
//            selected = nuevo;

            //Poner valor por defecto al ID de cada registros que se crea
            //(Tamaño de la lista + 1), se debe convertir a short
            //selected.setId((short) (items.size() + 1));
            //Poner valor por defecho al estado del usuario
            selected.setEstado("Si");

            //Establecer fecha de inicio de suscripción a la fecha actual.
            Date fechaInicio = new Date();
            selected.setFechaIngreso(fechaInicio);

            //Establecer fecha de fin de suscripción
            Date fechaFin = new Date();

            if (selected.getSuscripcionId().getId() == (short) 1) {
                fechaFin.setMonth(fechaFin.getMonth() + 1);
            } else if (selected.getSuscripcionId().getId() == (short) 2) {
                fechaFin.setMonth(fechaFin.getMonth() + 6);
            } else if (selected.getSuscripcionId().getId() == (short) 3) {
                fechaFin.setMonth(fechaFin.getMonth() + 12);
            }

            //Tipo de usuario por defecto: USUARIO
            selected.setTipoUsuarioId(ejbTipoUsuario.find((short) 2));

            selected.setFechaCorte(fechaFin);

            items = null;    // Invalidate list of items to trigger re-query.
        }

        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("UsuarioCreated"));
    }

    public String seleccionarMensual() {

        selected.setSuscripcionId(ejbSuscripcion.find((short) 1));
        crearYRetornar();

        return "volverInicio";
    }

    public String seleccionarSemestral() {
        selected.setSuscripcionId(ejbSuscripcion.find((short) 2));
        crearYRetornar();

        return "volverInicio";
    }

    public String seleccionarAnual() {
        selected.setSuscripcionId(ejbSuscripcion.find((short) 3));
        crearYRetornar();

        return "volverInicio";
    }

    public void inactivarUsuario() {
        if (selected.getEstado().equals("Si")) {
            selected.setEstado("No");
        } else {
            selected.setEstado("Si");
        }

        update();
    }

    //Metodo para crear el usuario nuevo y regresar a la pagina principal
    public void crearYRetornar() {
        //Ingresar registro a la base de datos
        create();

        //Vaciar valores para evitar duplicados
        selected.setNombre("");
        selected.setPassword("");
        selected.setCorreo("");

    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("UsuarioUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("UsuarioDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Usuario> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Usuario getUsuario(java.lang.Short id) {
        return getFacade().find(id);
    }

    public List<Usuario> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Usuario> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Usuario.class)
    public static class UsuarioControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            UsuarioController controller = (UsuarioController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "usuarioController");
            return controller.getUsuario(getKey(value));
        }

        java.lang.Short getKey(String value) {
            java.lang.Short key;
            key = Short.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Short value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Usuario) {
                Usuario o = (Usuario) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Usuario.class.getName()});
                return null;
            }
        }

    }

    //Metodo para redireccionar desde la pagina de registrar usuario hasta la pagina para seleccionar el tipo de suscripcion
    public String irSuscripcion() {

        return "irSuscripcion";

    }

}
