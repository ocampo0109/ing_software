/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.ingenieria_soft.controlador;

import co.com.ingenieria_soft.entidades.Usuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Jhonatan
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> {

    @PersistenceContext(unitName = "ingenieria_softPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }
    
    //Crear metodos para usuario
    
    public Usuario encontrarUsuarioPorCorreo(String correo){
        Query q = em.createNamedQuery("Usuario.findByCorreo", Usuario.class).setParameter("correo", correo);
        List<Usuario> list = q.getResultList();
        
        //Si la lista esta vacia no encontro ningun usuario con ese correo
        if (list.isEmpty()){
            return null;
        } else {
            //Por lo general, solo encuentra 1 usuario con el correo solicitado
            return list.get(0);
        }
        
    }
    
}
