/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.ingenieria_soft.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jhonatan
 */
@Entity
@Table(name = "tipo_hosteria")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoHosteria.findAll", query = "SELECT t FROM TipoHosteria t")
    , @NamedQuery(name = "TipoHosteria.findById", query = "SELECT t FROM TipoHosteria t WHERE t.id = :id")
    , @NamedQuery(name = "TipoHosteria.findByDescripcion", query = "SELECT t FROM TipoHosteria t WHERE t.descripcion = :descripcion")})
public class TipoHosteria implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Short id;
    @Size(max = 100)
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(mappedBy = "tipoHosteria")
    private List<Hosteria> hosteriaList;

    public TipoHosteria() {
    }

    public TipoHosteria(Short id) {
        this.id = id;
    }

    public Short getId() {
        return id;
    }

    public void setId(Short id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<Hosteria> getHosteriaList() {
        return hosteriaList;
    }

    public void setHosteriaList(List<Hosteria> hosteriaList) {
        this.hosteriaList = hosteriaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoHosteria)) {
            return false;
        }
        TipoHosteria other = (TipoHosteria) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return descripcion;
    }
    
}
